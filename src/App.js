import './App.css';
import ItemCard from './components/ItemCard';
import "bootstrap/dist/css/bootstrap.min.css"
import { AllUsers } from './pages/AllUsers';
import MyNavBar from './components/MyNarBar';
import { AllPhotos } from './pages/AllPhotos';
 
function App() {

  
  return (
    <div  >
      <MyNavBar/> 
      <AllPhotos/>
      {/* <AllUsers/> */}
      {/* <ItemCard/> */}
    </div>
  );
}

export default App;
